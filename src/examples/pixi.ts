import { effect, Ref } from '@vue/reactivity';
import { Application, Text } from 'pixi.js';

class ReactiveCounter extends Text {
  constructor(textRef: Ref<number>) {
    super('');

    this.anchor.set(0.5);
    this.style.align = 'center';

    effect(() => {
      this.text = `Counter value: ${textRef.value}`;
    });
  }
}

export function init(counterRef: Ref<number>): void {
  const container = document.querySelector<HTMLDivElement>('#app')!;
  const app = new Application({
    width: 640,
    height: 480,
    backgroundColor: 0xffffff,
  });

  const textObject = new ReactiveCounter(counterRef);

  app.stage.addChild(textObject);
  container.append(app.view);

  function loop() {
    const t = Date.now() / 1000;

    textObject.position.set(
      app.screen.width / 2 + Math.cos(t) * 50,
      app.screen.height / 2 + Math.sin(t) * 50
    );

    app.render();

    requestAnimationFrame(loop);
  }

  loop();
}