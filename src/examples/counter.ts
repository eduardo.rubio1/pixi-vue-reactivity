import { effect, Ref } from '@vue/reactivity'

export function init(counterRef: Ref<number>): void {
  const counter = document.querySelector<HTMLSpanElement>('#counter')!;
  const addButton = document.querySelector<HTMLButtonElement>('#addButton')!;
  const substractButton = document.querySelector<HTMLButtonElement>('#substractButton')!;

  const counterEffect = effect(() => {
    counter.innerText = `${counterRef.value}`;
  });

  addButton.addEventListener('click', () => {
    counterRef.value++;
  });

  substractButton.addEventListener('click', () => {
    counterRef.value--;
  });

  // dispose the effect with `stop(counterEffect)`
}