import './style.css'
import { ref } from '@vue/reactivity';

import { init as initCounter } from './examples/counter';
import { init as initPixiApp } from './examples/pixi';

const counterRef = ref(0);

initCounter(counterRef);
initPixiApp(counterRef);

setInterval(() => {
  counterRef.value++;
}, 1000);
